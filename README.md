carrito-de-compras-facorit

Este proyecto es un sistema de comercio electrónico desarrollado en Angular. Permite a los usuarios explorar productos, realizar compras, y gestionar sus pedidos. La aplicación también incluye características de carrito de compras.

Requisitos

    Node.js y npm instalados
    Angular CLI instalado (npm install -g @angular/cli)

Instalación

    Clona el repositorio: https://gitlab.com/QuiqueGomez/carrito-de-compras-facorit.git
    Navega al directorio del proyecto: cd carrito-de-compras
    Instala las dependencias: npm install

Ejecución

    Inicia el servidor de desarrollo: ng serve
    Abre tu navegador y visita http://localhost:4200/

Características

    Exploración de Productos: Los usuarios pueden navegar por una amplia gama de productos, filtrar por categoría, y ver detalles individuales de los productos.

    Carrito de Compras: Los usuarios pueden agregar productos al carrito y ver su carrito actual.


Tecnologías Utilizadas

    Angular
    TypeScript
    HTML/CSS
    Tailwind
