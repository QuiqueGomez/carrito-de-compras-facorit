export interface Cart {
    items: Array<CartItem>;
}   

export interface CartItem {
    img: string;
    quantity: number;
    name:string;
    price:number;
    id:number;
}